import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-text-banner',
  templateUrl: './text-banner.component.html',
  styleUrls: ['./text-banner.component.css']
})
export class TextBannerComponent {

  @Input() 
  title!: string;

}
