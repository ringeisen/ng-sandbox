import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, Input } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  animations: [

    trigger('appear', [
      // ...
      state('hidden', style({
        transform: 'scale(1.05)',
        opacity: 0
      })),
      state('visible', style({
        transform: 'scale(1)',
        opacity: 1
      })),
      transition('hidden => visible', [
        animate('1.5s')
      ])
    ]),
  ]

})
export class WelcomeComponent
implements AfterViewInit {

  @Input() 
  title!: string;

  @Input()
  logo: string | undefined;

  @Input()
  actionLink: string | undefined;

  @Input()
  actionText: string | undefined;

  headerVisible = false;

  buttonVisible = false;

  ngAfterViewInit(): void {

    setTimeout(() => {
      this.headerVisible = true;
    }, 500);

    setTimeout(() => {
      this.buttonVisible = true;
    }, 1000);

  }
}
