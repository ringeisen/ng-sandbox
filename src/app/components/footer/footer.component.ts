import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {

  @Input() 
  twitter!: string;

  @Input() 
  facebook!: string;

  @Input() 
  instagram!: string;

  @Input() 
  dribble!: string;

  @Input() 
  github!: string;

  @Input() 
  copyright!: string;

}
